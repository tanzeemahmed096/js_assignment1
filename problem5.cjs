module.exports = function problem5(allCarYear){
  if(arguments.length === 0) return [];

  if(!Array.isArray(allCarYear)) return [];

  const resArr = [];
  for(let idx = 0; idx < allCarYear.length; idx++){
    if(allCarYear[idx] < 2000) resArr.push(allCarYear[idx]);
  }

  return [resArr, resArr.length];
}