module.exports = function problem4(inventory){
  if(arguments.length === 0) return [];

  if(typeof inventory !== "object" || inventory.length === 0) return [];

  const carYear = [];

  for(let idx = 0; idx < inventory.length; idx++){
    if(inventory[idx].car_year) carYear.push(inventory[idx].car_year);
  }

  return carYear;
}