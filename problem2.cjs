module.exports = function problem2(inventory){
  if(arguments.length === 0) return [];

  if(Array.isArray(inventory) && inventory.length === 0) return [];

  if(!Array.isArray(inventory)) return inventory;

  return inventory[inventory.length - 1];
}