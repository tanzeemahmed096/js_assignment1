//Importing the car array of objects from car.js
const carsDetail = require("./car.cjs");

//Importing problem5 statement logic
const problem5 = require("./problem5.cjs");

//Importing all year array
const years = require("./problem4.cjs");
const allYears = years(carsDetail);

//Storing the array of old cars;
const oldCars = problem5(allYears);

if (oldCars.length === 0) {
  console.log("Years not available for car");
} else {
  for (let idx = 0; idx < oldCars.length; idx++) {
    console.log(oldCars[idx] + "\n");
  }
}
