module.exports = function problem1(inventory, id){
  if(arguments.length === 0) return [];

  if(Array.isArray(inventory) && inventory.length === 0) return [];

  if(id === undefined || typeof id !== 'number') return [];

  if(!Array.isArray(inventory)){
    if(!inventory.id || !inventory.car_make || !inventory.car_model || !inventory.car_year) return [];
    else return inventory;
  }
  
  for(let idx = 0; idx < inventory.length; idx++){
    if(inventory[idx].id === id) return inventory[idx];
  }
  
  return [];
}
