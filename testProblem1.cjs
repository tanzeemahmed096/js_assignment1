//Importing the car array of objects from car.js
const carsDetail = require("./car.cjs");

//Importing the problem1 logic
const problem1 = require("./problem1.cjs");

//Storing car information with id 33 in a variable
const carInfo = problem1([{"id":0,"car_make":"Dodge","car_model":"Magnum","car_year":2008}], 0);

//Displaying the output
if (carInfo.length === 0) {
  console.log("Provide the inventory or no data found");
} else {
  console.log(
    `car 33 is a ${carInfo.car_year} ${carInfo.car_make} ${carInfo.car_model}`
  );
}
