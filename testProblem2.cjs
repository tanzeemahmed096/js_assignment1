//Importing the car array of objects from car.js
const carsDetail = require("./car.cjs");

//Importing problem2 statement logic
const problem2 = require("./problem2.cjs");

//Storing last car information in a variable
const lastCarInfo = problem2(carsDetail);

//Displaying the output
if (lastCarInfo.length === 0) {
  console.log("Inventory is empty");
} else {
  console.log(`Last car is a ${lastCarInfo.car_make} ${lastCarInfo.car_model}`);
}
