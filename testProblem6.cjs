//Importing the car array of objects from car.js
const carsDetail = require("./car.cjs");

//Importing problem6 statement logic
const problem6 = require("./problem6.cjs");
const cars = problem6(carsDetail);

//Consoling the results
if(cars.length === 0){
  console.log("No BMW and Audi car found or inventory is empty");
}else{
  console.log(cars);
}
