//Importing the car array of objects from car.js
const carsDetail = require("./car.cjs");

//Importing problem4 statement logic
const problem4 = require("./problem4.cjs");

//Storing the array of all years
const carYear = problem4(carsDetail);

//Consoling the result
if (carYear.length === 0) {
  console.log("Inventory is empty");
} else {
  for (let idx = 0; idx < carYear.length; idx++) {
    console.log(carYear[idx] + "\n");
  }
}
