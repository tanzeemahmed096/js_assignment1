//Importing the car array of objects from car.js
const carsDetail = require("./car.cjs");

//Importing problem3 statement logic
const problem3 = require("./problem3.cjs");

//Storing car model names sorted aplhabetically in a variable
const carModel = problem3(carsDetail);

//conslose the result
if (carModel.length === 0) {
  console.log("Invertory is empty");
} else {
  for (let idx = 0; idx < carModel.length; idx++) {
    console.log(carModel[idx] + "\n");
  }
}
