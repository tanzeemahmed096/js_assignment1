module.exports = function problem6(inventory){
  if(arguments.length === 0) return [];

  if(typeof inventory !== 'object') return [];
  
  const cars = [];
  for(let idx = 0; idx < inventory.length; idx++){
    if (inventory[idx].car_make === "BMW" || inventory[idx].car_make === "Audi") {
      cars.push(inventory[idx]);
    }
  }
  
  return JSON.stringify(cars);
}