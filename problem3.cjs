module.exports = function problem3(inventory){
  if(arguments.length === 0) return [];

  if(typeof inventory !== "object" || inventory.length === 0) return [];

  const carModelArr = [];

  for(let idx = 0; idx < inventory.length; idx++){
    if(inventory[idx].car_model) carModelArr.push(inventory[idx].car_model);
  }

  carModelArr.sort();
  return carModelArr;
}